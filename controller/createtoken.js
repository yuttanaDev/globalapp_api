const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

//const jwt = require("jwt-simple");
const jwt = require('jsonwebtoken');  // ใช้งาน jwt module
const fs = require('fs')
const Config = require('../model/config');

router.use(bodyParser.json());


router.get('/', (req, res)=> {
    //res.send('<h1>Hello Node.js</h1>');
    res.json({status: 404,msg:"url not found."});
});

router.get('/gettoken',  (req, res)=> {
    const payload = {
            sub: "514244",
            iat: new Date().getTime()//มาจากคำว่า issued at time (สร้างเมื่อ)
        };
     // const SECRET_KEY = fs.readFileSync('./certificate/private.key','utf8'); // fs.readFileSync(__dirname+'/../config/private.key') ,Config.setting.secret_key
     const SECRET_KEY = Config.setting.secret_key; 
    
    const jwtoption =  {algorithm:  "HS256" }; //expiresIn:  "12h", HS256 =ganaral KEY , RS256  = private & publick KEY

    jwt.sign(payload, SECRET_KEY,jwtoption,(err,token)=>{
          if(err){
            res.json({status:404,token:err});
          }else{
            res.json({status:200,token:token});
             /* res.send(jwt.encode(payload, SECRET_KEY)); */  // simple jwt  
          }
    });


});

module.exports = router;