const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const { Pool } = require('pg');
const sql = require("sql-query");
const sqlQuery = sql.Query("postgresql");

const Db = require("../../model/db.js");

//const auth = new Auth();
router.use(bodyParser.json());

//index page
router.get("/", (req, res) => {
  //res.send('<h1>Hello Node.js</h1>');
  res.json({ status: 404, msg: "url not found." });
});

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */

getDataReport();
/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */

async function getDataReport(){
    //report
    await router.post("/reportrequst",async (req,res) => {
        const { productcode,brchcode} = req.body;
        ;(async () => {
            const pg_rds = await Db.pg_rds.connect()
            try {
             
              var qstr =
                        " SELECT  \n"+
                        " x.srcbrchcode, \n"+
                        " x.desbrchcode, \n"+
                        " x.productcode, \n"+
                        " x.productname, \n"+
                        " x.unitcount, \n"+
                        " x.transferqty \n"+
                        " FROM reports.mv_trs_transferout as x \n"+
                        // " WHERE x.productcode = '"+ productcode +"' \n"+
                        // " AND x.desbrchcode = '"+brchcode+"' \n"+
                        " ORDER BY x.srcbrchcode DESC LIMIT 50";
      
            pg_rds.query(qstr, async function(err, result) {
                        if(err){
                           return res.json({status: 405,title: "ไม่สำเร็จ", 
                           subTitle: "ไม่สำเร็จ ไม่สามารถดึงข้อมูลได้ " + err });
                        }else{
                          if (result.rowCount > 0) {
      
                            const callback = [];
                            const dateNow = new Date();
                            result.rows.map((data, index) => {
                              callback[index] = {
                                srcbrchcode: data.srcbrchcode,
                                desbrchcode: data.desbrchcode,
                                productcode: data.productcode,
                                unitcount: data.unitcount,
                                transferqty: data.transferqty,
                              };
                            });
      
                            return res.json({
                              status: 200,
                              data: callback,
                              title: "สำเร็จ", 
                              subTitle: "สำเร็จ สามารถดึงข้อมูลมาสำเร็จ" ,
                            });
                           
                            //return res.send(info);
                            
                          } else {
                            return res.json({ status: 404, data: null, title: "ไม่มีรหัสผ่านของท่าน", subTitle: "ไม่มีรหัสผ่านของท่าน กรุณาลองใหม่อีกครั้ง" });
                          }
                        }
                    });
      
                    //console.log('callback',callback);
            } finally {
              pg_rds.release();
            }
          })(500).catch(err => {
            return res.json({status: 408,title: "ไม่มีสามารถ Connect DB ได้", subTitle: "ไม่มีสามารถ Connect DB ได้ กรุณาลองใหม่อีกครั้ง" }) 
          });
           
    }); 
}



module.exports = router;
