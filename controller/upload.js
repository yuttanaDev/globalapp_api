const express = require("express");
const router = express.Router();
var bodyParser = require("body-parser");
var multer = require("multer");
var fs = require("fs");


const sql = require("sql-query");
const sqlQuery = sql.Query("postgresql");

const Db = require("../model/db.js");
const auth = require("../model/authenmodel.js");
const jwtauthen = require("../model/authenjwt");

//const auth = new Auth();
router.use(bodyParser.json());

//index page
router.get("/", (req, res) => {
  //res.send('<h1>Hello Node.js</h1>');
  res.json({ status: 404, msg: "url not found." });
});

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */

getData();

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */
async function getData() {
  const uploadImage = async (req, res, next) => {
    try {
        // to declare some path to store your converted image
        const path = './uploads/'+Date.now()+'.png'
        const imgdata = req.body.base64image;
        // to convert base64 format into random filename
        const base64Data = imgdata.split("base64,")[1];
        
        fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
        return res.send(path);
    } catch (e) {
        next(e);
    }
  }
  await router.post("/image", uploadImage);
}



module.exports = router;
