const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

const sql = require("sql-query");
const sqlQuery = sql.Query("postgresql");

const Db = require("../model/db.js");
const auth = require("../model/authenmodel.js");
const jwtauthen = require("../model/authenjwt");

//const auth = new Auth();
router.use(bodyParser.json());

//index page
router.get("/", (req, res) => {
  //res.send('<h1>Hello Node.js</h1>');
  res.json({ status: 404, msg: "url not found." });
});

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */

getData();

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */
async function getData() {
  /*await หน้า function promise ที่ต้องการให้รอ*/
  //Login Master
  await router.get("/brachall", jwtauthen.authorMidleware, (req, res) => {
    //const { username, userpass } = req.body;

    ;(async () => {
      const pg_gmb = await Db.pg_gmb.connect()
      try {
        
      var qstr =
        " SELECT branchcode,branchshorten,branchname ";
      qstr += " FROM master_data.vw_master_branch_active ";
      qstr += " ORDER BY branchcode ASC ";
      pg_gmb.query(qstr, function(err, result) {
                  if(err){
                     return res.json({status: 405,msg:err.detail});
                  }else{
                    if (result.rowCount > 0) {
                      // callback Data
                      //const callback = result.rows;
                      // make new format callback Data
                      const callback = [];
                      const dateNow = new Date();
                      result.rows.map((data, index) => {
                        callback[index] = {
                          branchcode: data.branchcode,
                          branchname: data.branchname,
                          branchshorten: data.branchshorten,
                          datenow:dateNow
                        };
                      });

                     
                      //return res.send(info);
                      return res.json({
                        status: 200,
                        data: callback,
                        msg: "data load done."
                      });
                    } else {
                      return res.json({ status: 404, data: null, msg: "no data" });
                    }
                  }
              });
      } finally {
        pg_gmb.release();
      }
    })(500).catch(err => {
      return res.json({status: 408,msg:"Request Time-out."}) 
    });

  });


}


module.exports = router;
