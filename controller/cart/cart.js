const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const { Pool } = require('pg');
const sql = require("sql-query");
const sqlQuery = sql.Query("postgresql");

const Db = require("../../model/db.js");

//const auth = new Auth();
router.use(bodyParser.json());

//index page
router.get("/", (req, res) => {
  //res.send('<h1>Hello Node.js</h1>');
  res.json({ status: 404, msg: "url not found." });
});

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */

getDataLogin();
getDataSarchprice();
getDataStockAll();

/*เมื่อต้องการใช้ await ประกาศ async หน้า function เสมอ */
async function getDataLogin() {
  //Login Master
  await router.post("/login",async (req, res) => {
    const { username, userpass } = req.body;


   

    ;(async () => {
      const pg_emp = await Db.pg_emp.connect()
      try {
       
      var qstr =
        " SELECT employeecode AS username,PASSWORD AS userpassword,firstname,lastname,newbranch,branchnamesubstr";
      qstr +=
        ",sectioncode,sectionname,positioncode,positionname,branchshorten,divisioncode";
      qstr += " FROM public.view_employee_mobile ";
      qstr += " WHERE employeecode = '" +username +"' AND PASSWORD = '" +userpass + "' ";
      pg_emp.query(qstr, async function(err, result) {
                  if(err){
                     return res.json({status: 405,title: "ไม่สำเร็จ", 
                     subTitle: "ไม่สำเร็จ ไม่สามารถดึงข้อมูลได้ " + err });
                  }else{
                    if (result.rowCount > 0) {

                      var qstr2 =
                      " SELECT aa.employeecode,aa.type_admin";
                      qstr2 += " FROM guide_product.admin_emp AS aa ";
                      qstr2 += " WHERE aa.employeecode = '" +username +"' ";
                    const client =  await Db.pg_gbm.connect()
                    const result2 = await  client.query(qstr2)
                   
                   // console.log(result2.rows) // [ [ 1, 2 ] ]
                    await client.end()
                    var employeecode= null ;
                    var type_admin= 1 ; 
                    if(result2.rowCount > 0){
                      employeecode = result2.rows[0].employeecode;
                      type_admin = result2.rows[0].type_admin;
                    }
                    //console.log(result2.rowCount,employeecode)

                    if(employeecode != null){

                      const callback = [];
                      const dateNow = new Date();
                      result.rows.map((data, index) => {
                        callback[index] = {
                          username: data.username,
                          userpassword: data.userpassword,
                          firstname: data.firstname,
                          lastname: data.lastname,
                          fullname: data.firstname + " " + data.lastname,
                          branchcode: data.newbranch,
                          branchname: data.branchnamesubstr,
                          sectioncode: data.sectioncode,
                          sectionname: data.sectionname,
                          positioncode: data.positioncode,
                          positionname: data.positionname,
                          branchshorten: data.branchshorten,
                          divisioncode: data.divisioncode,
                          type_admin: type_admin,
                          type_tatus: 1,
                        };
                      });

                      return res.json({
                        status: 200,
                        data: callback,
                        title: "สำเร็จ", 
                        subTitle: "สำเร็จ สามารถดึงข้อมูลมาสำเร็จ" 
                      });

                    }else{

                      const callback = [];
                      const dateNow = new Date();
                      result.rows.map((data, index) => {
                        callback[index] = {
                          username: data.username,
                          userpassword: data.userpassword,
                          firstname: data.firstname,
                          lastname: data.lastname,
                          fullname: data.firstname + " " + data.lastname,
                          branchcode: data.newbranch,
                          branchname: data.branchnamesubstr,
                          sectioncode: data.sectioncode,
                          sectionname: data.sectionname,
                          positioncode: data.positioncode,
                          positionname: data.positionname,
                          branchshorten: data.branchshorten,
                          divisioncode: data.divisioncode,
                          type_admin: 1,
                          type_tatus: 1,
                        };
                      });

                      return res.json({
                        status: 200,
                        data: callback,
                        title: "สำเร็จ", 
                        subTitle: "สำเร็จ สามารถดึงข้อมูลมาสำเร็จ" 
                      });

                    }
                     
                      //return res.send(info);
                      
                    } else {
                      return res.json({ status: 404, data: null, title: "ไม่มีรหัสผ่านของท่าน", subTitle: "ไม่มีรหัสผ่านของท่าน กรุณาลองใหม่อีกครั้ง" });
                    }
                  }
              });

              //console.log('callback',callback);
      } finally {
        pg_emp.release();
      }
    })(500).catch(err => {
      return res.json({status: 408,title: "ไม่มีสามารถ Connect DB ได้", subTitle: "ไม่มีสามารถ Connect DB ได้ กรุณาลองใหม่อีกครั้ง" }) 
    });

   
  }); 
}



  //sarchprice
async function getDataSarchprice() {

  await router.post("/sarchprice",async (req, res) => {

    (async () => {
      const {
        branchcode,
        serchtypeid,
        barcode,
        productcode,
        productname,
        limitdata,
        selectall,
        isstock
      } = req.body;
  
      const pg_master = await Db.pg_master.connect();
  
      try {
        var sqlBranch =
          "SELECT\n" +
          "	ip_address,\n" +
          "	database_name,\n" +
          "	branchname,\n" +
          "	branchcode,\n" +
          "	dbusername,\n" +
          "	dbpassword \n" +
          "FROM\n" +
          "	SECURITY.branchserver_fixip \n" +
          "WHERE\n" +
          "	branchcode = '" +
          branchcode +
          "'";
  
          pg_master.query(sqlBranch, (err, result) => {
          if (err) {
            res.json({ status: 400,title:err.stack, subTitle: err.stack, rows: null,displaycode:null,incentive: null });
          } else {
            var playload;
            result.rows.map((data, index) => {
              playload = {
                ip_address: data.ip_address,
                database_name: data.database_name,
                branchname: data.branchname,
                branchcode: data.branchcode,
                dbusername: data.dbusername,
                dbpassword: data.dbpassword
              };
            });
  
            var clientPos = new Pool({
              host: playload.ip_address,
              database: playload.database_name,
              user: playload.dbusername,
              password: playload.dbpassword,
              port: 5432
            });
            clientPos.connect();
  
            var sql =
              " SELECT\n" +
              "	aa.product_code,\n" +
              "	aa.barcode_code,\n" +
              "	aa.barcode_bill_name,\n" +
              "	aa.unit_code,\n" +
              "	aa.stockonsale :: NUMERIC ( 15, 2 ),\n" +
              "	aa.unit_rate :: NUMERIC ( 15, 2 ),\n" +
              "	aa.product_price1 :: NUMERIC ( 15, 2 ),\n" +
              "	aa.product_price2 :: NUMERIC ( 15, 2 ),\n" +
              "	aa.product_price3 :: NUMERIC ( 15, 2 ),\n" +
              "	aa.pro_price :: NUMERIC ( 15, 2 ),\n" +
              "	aa.stockbalance :: NUMERIC ( 15, 2 ),\n" +
              "	aa.item_r11 :: NUMERIC ( 15, 2 ), \n" +
              "	bb.product_path_internet AS picturename, \n" +
              "	( CASE WHEN cc.product_code IS NULL THEN 'A' ELSE 'I' END ) AS product_acticve \n" +
              " FROM  stockcard.vw_searchprice AS aa \n" +
              " LEFT JOIN PUBLIC.temp_master_product_img_new AS bb ON aa.barcode_code = bb.product_code \n" +
              " LEFT JOIN PUBLIC.product_inactive AS cc ON aa.barcode_code = cc.product_code \n" +
              " WHERE  \n";
            if (serchtypeid == "1") {
              if (selectall == "TRUE") {
                sql += "	aa.barcode_code LIKE '%" + barcode + "%' \n";
              } else {
                sql += "	aa.barcode_code = '" + barcode + "' \n";
              }
            } else if (serchtypeid == "2") {
              if (selectall == "TRUE") {
                sql += "	aa.product_code LIKE '%" + productcode + "%' \n";
              } else {
                sql += "	aa.product_code = '" + productcode + "' \n";
              }
            } else if (serchtypeid == "3") {
              if (selectall == "TRUE") {
                sql += "	aa.barcode_bill_name LIKE '%" + productname + "%' \n";
              } else {
                sql += "	aa.barcode_bill_name = '" + productname + "' \n";
              }
            }
  
            if (isstock == "TRUE") {
              sql += "  AND aa.stockonsale::numeric(15,2) > 0  \n";
            }
            sql += "	LIMIT " + limitdata + " ";
  
            clientPos.query(sql, async (err, result) => {
              if (err) {
                res.send({
                  status_code: "400",
                  status_name: "เกิดข้อผิดพลาด",
                  data: "null"
                });
              } else {
                if (result.rowCount > 0) {

                  /* var qstr2 =
                  " SELECT\n" +
                  "	incentive_cashback_product_barcode,\n" +
                  "	incentive_cashback_product_compensation,\n" +
                  "	incentive_cashback_type_name,\n" +
                  "	date_start,\n" +
                  "	date_stop\n" +
                  " FROM  PUBLIC.incentive_cashback_product C \n" +
                  " LEFT JOIN PUBLIC.incentive_cashback_type T ON C.incentive_cashback_type_id = T.incentive_cashback_type_id  \n" +
                  " WHERE  \n" +
                  "	now( ) :: DATE >= date_start :: DATE \n" +
                  "	AND now( ) :: DATE <= date_stop :: DATE \n" +
                  "	AND status_doc <> 'C'  \n" +
                  "	AND incentive_cashback_type_active = TRUE  \n" +
                  "	AND incentive_cashback_product_active = TRUE \n" +
                  "	AND incentive_cashback_product_barcode = '"+ productcode +"'  \n" 
                  ;
                const pg_one =  await Db.pg_one.connect()
                const result2 = await  pg_one.query(qstr2)
               
                await pg_one.end()
                var incentive = null ;
                if(result2.rowCount > 0){
                  incentive = result2.rows;
                } */
                
                  return res.json({
                    status: 200,
                    rows: result.rows,
                    displaycode:null,
                    incentive: null,
                    title:'ไม่พบสินค้า',
                    subTitle: 'ไม่พบสินค้า, หรือให้ทำการแสกนใหม่อีกครั้ง!'
                  });
                } else {
                  return res.json({
                    status: 400,
                    rows: null,
                    displaycode:null,
                    incentive: null,
                    title:'ไม่พบสินค้า',
                    subTitle: 'ไม่พบสินค้า, หรือให้ทำการแสกนใหม่อีกครั้ง!'
                  });
                }
              }
            });
            clientPos.end();
          }
        });
      } finally {
        pg_master.release();
      }
    })().catch(err => {
      res.json({ status: 408,  title:err.stack, subTitle: err.stack, rows: null,displaycode:null,incentive: null });
    });

  }); 
}




async function getDataStockAll() {
  //Login Master
  await router.post("/stockall", async (req, res) => {
    const { productcode, branchcode,county } = req.body;

    ;(async () => {
      const pg_rds = await Db.pg_rds.connect()
      try {
       
        var qstr =
        " SELECT \n " +
        " y.branch_code brchcode, \n " +
        " y.branch_short_name, \n " +
        " ( CASE WHEN x.productcode IS NOT NULL THEN x.productcode ELSE '"+ productcode +"' END ) productcode, \n " +
        " ( CASE WHEN x.stockqty IS NOT NULL THEN x.stockqty ELSE 0 END ) :: NUMERIC ( 18, 2 ) stockqty, \n "+ 
        " (CASE \n " +
        " WHEN y.branch_short_name IN ( 'TP ', 'CN', 'AT', 'AY', 'SY', 'PT', 'SH', 'DKT' ) THEN \n " +
        " 0 ELSE 1 \n " +
        " END \n " +
        " ) runcar_dc, \n " +
        " COALESCE ( mat60.saleqty, 0 ) :: NUMERIC ( 18, 2 ) saleqty, \n " +
        " COALESCE ( mat60.saleamnt, 0 ) :: NUMERIC ( 18, 2 ) saleamnt \n " +
        " FROM \n " +
        " masterdata.master_branch y \n " +
        " LEFT JOIN ( \n " +
        " SELECT \n " +
        "  i.brchcode, \n " +
        "  i.productcode, \n " +
        " i.stockqty  \n " +
        " FROM \n " +
        " dblink ( 'dbname=globalhousedb port=5432 host=192.168.0.8 user=root password=Gl0B@lH0use290363P@ssw0rd2020', ' \n " +
        " SELECT brchcode, productcode, stockqty from supplychain.fc_stock_erp_realtime_mobile(''"+ productcode +"'');' ) AS i (  \n " +
        "  brchcode VARCHAR ( 50 ),productcode VARCHAR ( 50 ), \n " +
        " stockqty NUMERIC ( 18, 2 )) \n " +
        " ) x ON x.brchcode = y.branch_code \n " +
        " AND y.branch_active = 't' \n " +
        " LEFT JOIN saledata.mat_sale_60day mat60 ON x.brchcode = mat60.branch_code \n " +
        " AND mat60.product_code = '"+ productcode +"' \n " +
        " WHERE \n " +
        " y.branch_active = 't' \n " +
        " AND y.branch_code NOT IN ( 'GH-999' ) \n " +
        " ORDER BY \n"+
        " y.branch_code \n" ;

      pg_rds.query(qstr, async function(err, result) {
                  if(err){
                     return res.json({status: 405,title: "ไม่สำเร็จ", 
                     subTitle: "ไม่สำเร็จ ไม่สามารถดึงข้อมูลได้ " + err });
                  }else{
                    if (result.rowCount > 0) {

                      const callback = [];
                      const dateNow = new Date();
                      result.rows.map((data, index) => {
                        callback[index] = {
                          brchcode: data.brchcode,
                          branch_short_name: data.branch_short_name,
                          productcode: data.productcode,
                          stockqty: data.stockqty,
                          runcar_dc: data.runcar_dc,
                          saleqty: data.saleqty,
                          saleamnt: data.saleamnt
                        };
                      });

                      return res.json({
                        status: 200,
                        data: callback,
                        title: "สำเร็จ", 
                        subTitle: "สำเร็จ สามารถดึงข้อมูลมาสำเร็จ" ,
                      });
                     
                      //return res.send(info);
                      
                    } else {
                      return res.json({ status: 404, data: null, title: "ไม่มีรหัสผ่านของท่าน", subTitle: "ไม่มีรหัสผ่านของท่าน กรุณาลองใหม่อีกครั้ง" });
                    }
                  }
              });

              //console.log('callback',callback);
      } finally {
        pg_rds.release();
      }
    })(500).catch(err => {
      return res.json({status: 408,title: "ไม่มีสามารถ Connect DB ได้", subTitle: "ไม่มีสามารถ Connect DB ได้ กรุณาลองใหม่อีกครั้ง" }) 
    });

   
  }); 
}






module.exports = router;
