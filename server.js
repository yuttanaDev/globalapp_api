/* โหลด Express มาใช้งาน */
const express = require("express");
const server = express();
const bodyParser = require("body-parser");
//const http = require("http").Server(server);
const cors = require('cors');

/* กำหนด port */
var PORT = process.env.PORT || 3030;

server.use(cors());

// Add headers
server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods','POST, GET, PUT, PATCH, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers','Content-Type, Option, Authorization')
  return next();
});

// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
server.use(bodyParser.json());

//root index
server.get("/", (req, res) => {
  res.send("Moblie-API Running....");
});

/*rount controll*/
const routes = require("./routes");
server.use(routes);

server.all("*", function(req, res) {
  res.status(403).send("403 - ไม่ได้รับอนุญาต ไม่มี API URL นี้");
});

server.listen(PORT, function(err, result) {
  if (err) throw err;
  console.log(`> Ready on Port:${PORT}`);
});


//const io = require("socket.io").listen(app);
//Socket.io
/* const io = (module.exports.io = require("socket.io")(http).listen(app));
io.attach(3000, {
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});
const SocketManager = require("./controller/SocketManager");
io.on("connection", SocketManager); */

