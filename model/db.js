const { Pool } = require('pg');
//const conn ='postgresql://postgres:prApH!6eXo@159.138.229.84:5432/gbh_erp';


/* ======= CONNENT pg_master ====== */

const connMASTER = {
    host: "159.138.233.247",
    database: "pgpos_global",
    user: "pgget",
    password: "PHimlxU@r##AyLch-VIg#-pUbrine=ACH!DR+bIdr2DRe&_p$n-wRasta_Re_rOb",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};

const pg_master = new Pool(connMASTER);

/* ======= CONNENT pg_master ====== */

/* ======= CONNENT POS ====== */
const connPOS = {
    user: "postgres",
    host: "159.138.229.84",
    database: "gbh_pos",
    password: "prApH!6eXo",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_pos = new Pool(connPOS);

/* ======= CONNENT POS ====== */

/* ======= CONNENT ERP ====== */
const connERP = {
    user: "postgres",
    host: "159.138.229.84",
    database: "gbh_erp",
    password: "prApH!6eXo",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_erp = new Pool(connERP);
/* ======= CONNENT ERP ====== */

/* ======= CONNENT EMP ====== */
const connEMP = {
    user: "root",
    host: "159.138.234.74",
    database: "globalhouseemployeedb",
    password: "1HOhu@oS@huawei",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_emp = new Pool(connEMP);
/* ======= CONNENT EMP ====== */

/* ======= CONNENT GMB ====== */
//host: "192.168.1.171",
//host: "110.49.11.36", 
const connGMB = {
    user: "postgres",
    host: "110.49.11.36", 
    database: "global_mobile",
    password: "p@ssw0rd456789",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_gmb = new Pool(connGMB);
/* ======= CONNENT GMB ====== */


/* ======= CONNENT ONE ====== */
 
const connONE = {
    user: "postgres",
    host: "192.168.1.123", 
    database: "pgpos_global",
    password: "p@ssw0rd",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_one = new Pool(connONE);
/* ======= CONNENT ONE ====== */

/* ======= CONNENT ERP ====== */
const connrds = {
    user: "root",
    host: "159.138.232.23",
    database: "bi",
    password: "1HOhu@oS@huawei",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_rds = new Pool(connrds);
/* ======= CONNENT ERP ====== */

/* ======= CONNENT connGBM ====== */
 //192.168.0.23 local
 //159.138.232.23
const connGBM = {
    user: "root",
    host: "192.168.0.23",
    database: "global_mobile",
    password: "1HOhu@oS@huawei",
    port: 5432,
    //max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
};
const pg_gbm = new Pool(connGBM);
/* ======= CONNENT connGBM ====== */

module.exports = {
    pg_pos,
    pg_erp,
    pg_master,
    pg_emp,
    pg_gmb,
    pg_one,
    pg_rds,
    pg_gbm
};