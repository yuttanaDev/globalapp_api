const jwt = require('jsonwebtoken');
const fs = require('fs'); // ใช้งาน file system module ของ nodejs
const Config = require('../model/config');

// สร้าง middleware ฟังก์ชั่นสำหรับ verification token
const authorMidleware = (req, res, next) => {
    const { authorization } = req.headers;  // ดึงข้อมูล authorization ใน header
    // ถ้าไม่มีการส่งค่ามา ส่ง ข้อความ json พร้อม status 401 Unauthorized
    if(authorization===undefined) return res.status(401).json({
        "status": 401,
        "message": "Unauthorized"
    })   
    // ถ้ามีการส่งค่ามา แยกเอาเฉพาะค่า token จากที่ส่งมา 'Bearer xxxx' เราเอาเฉพาะ xxxx
    /* const token = req.headers['authorization'].split(' ')[1]
    if(token===undefined) return res.status(401).json({ // หากไมมีค่า token
        "status": 401,
        "message": "Unauthorized"
    }) */   
    // ใช้ค่า privateKey เ็น buffer ค่าที่อ่านได้จากไฟล์ private.key ในโฟลเดอร์ config
    //const publicKey = fs.readFileSync('./certificate/public.key','utf8'); //fs.readFileSync(__dirname+'/../config/private.key') ,Config.setting.secret_key
    const publicKey = Config.setting.secret_key;
    // ทำการยืนยันความถูกต้องของ token
    jwt.verify(authorization, publicKey, function(error, decoded) {
        if(error){
            return res.status(401).json({ // หาก error ไม่ผ่าน
                            "status": 401,
                            "message": "Invalid verify secret key"
                            
                        }) ;           
        }else{

        // ถ้าไม่ผ่าน เช่นไม่ได้ส่งค่า role มาด้วยหรือ ค่า role เป็น 'user' ไม่ใช่ 'admin' ก็จะส่งค่า status 403 Forbidden
        /* if(decoded.role===undefined || decoded.role!=='admin') return res.status(403).json({
            "status": 403,
            "message": "Forbidden"
        })  */  
        // ถ้าทุกอย่างผ่าน ทุกเงื่อนไข ก็ไปทำ middleware ฟังก์ชั่นในลำดับถัดไป
        next();
       }
    });
}
 
module.exports = {authorMidleware};