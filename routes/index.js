const server = require("express");
const router = server.Router();
const jwtauthen = require("../model/authenjwt");

//Master
const ctrlToken = require("../controller/createtoken");
const ctrlLinenoti = require("../controller/linenotify");
const ctrlMaster = require("../controller/master");
const ctrlUpload = require("../controller/upload");

//Product Ctrl
const ctrlProduct = require("../controller/product/routes");

//Report Ctrl
const ctrlReport = require("../controller/report/routes");

//Master
router.use("/api", ctrlToken);
router.use("/linenoti", ctrlLinenoti);
router.use("/api/master", jwtauthen.authorMidleware, ctrlMaster);
router.use("/api/upload", ctrlUpload);

//App Module
router.use("/api/product", jwtauthen.authorMidleware, ctrlProduct);

//Report Module
router.use("/api/report", ctrlReport);

module.exports = router;
 